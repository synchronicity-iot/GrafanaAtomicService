# Grafana Atomic Service

## Description
This baseline service is offering a custom deployment of Grafana software working 
on top of the FIWARE stack services of SynchroniCity.

Through this service, a set of databases can be configured as datasources in 
order to create personalized charts and tables. These databases are filled using 
QuantumLeap or FIWARE Cygnus as device information subscriber. Grafana provides 
a user management system allowing the creation and management of isolated 
dashboards with custom charts and device information.

In addition, customized plugins are included for different visualization of 
real-time and historical data on a map.

The challenges addressed by the tool are:
- Process data from multiple sources/sensors in real-time from SynchroniCity framework
- View flexibility to certain data sets (e.g. through dashboard widgets)
- Selection of specified time window and display corresponding aggregated data over the selected time window
- Annotation specified time windows
- User Interface is intuitive and easy to use

![alt text](https://synchronicity-iot.eu/wp-content/uploads/2019/02/metrics.png "Grafana dashboard")

## How to use it
Grafana provides guides covering the main topics: from deployment and configuration to panel and dashboard creation.
Grafana is a widely used tool with a strong community and for this reason is really easy to find documentation.

The custom Synchronicity map-based plugin provided in the context of the project counts on two guides. 
One describing the services environment and its configuration and another exposing the configuration in the Grafana 
graphical interface. Both guides are available in the repository main root.

## Interface and API
As shown in the previous figure, Grafana provides a graphical interface allowing
users to easily visualize the data in their deployments. 

### API
Postman collections covering the main functionalities of each of the services composing the architecture is provided. There are services not offering a API to interact with or providing an user interface, for this reasons the postman collection do not cover all the services deployed.

## Service Architecture
![_images/synchronicity_diagram.png](images/synchronicity_diagram.png)

The architecture of this service, is simple. QuantumLeap is in charge of the data received from Synchronicity framework context management API and store it in CrateDB.
Finally, Grafana will configure this CrateDB instance as datasource allowing the user to retrieve and create custom dashboard and panels. 

## Service Architecture
This repository presents a simple deployment based on the docker technology. In the _docker-compose/_ folder a docker-compose file
including all the services required can be encountered. 

### Docker Architecture
All the services in the architecture are deployed using a docker-compose file therefore the configuration options are explicitly declared in this file.
All the services share the subnet and will be reachable through the dns namespaces provided by docker.

# Build, deploy and run
The architecture building and execution must contains using docker-compose must contains a previous step. 

## Build architecture through docker-compose

Launch Atomic Service:

```
$ docker-compose up
```

Or launch service in background:
```
$ docker-compose up -d
```

Stop service:
```
$ docker-compose down
```

## Check container running

A simple way to test the atomic service is running on docker, is using the next command:
```
$ docker ps
```
Now appear four running services along with its attributes, this table contains ports, names, images of the docker containers. To obtain the data in a more compact form use:
```
$ docker ps --format 'Service: {{.Names}} \nPorts: {{.Ports}}\n'
```

## System requirements
Minimum requirements to run this atomic service:
  * docker
  * docker-compose

## Support
Once Metrics Visualizer (Grafana) atomic services has been deployed, it is important to keep update it, helping cities to deploy and integrate different data sources into their Grafana services.
For that reason, HOPU team offers a service support to Grafana services which is detailed below:
Which include
- Data loading in databases
- Integration with synchronicity framework
- Provide the guides and help to anyone interested in the tool
- Remote support via mail, phone explaining how to exploit data collected by different devices or application deployed around cities
- Solving functional mistakes
- Technical support
- Consultations about operation, use and exploitation of Grafana
Which not include
- The development of new modules which add new functionalities to Grafana
- Redesign of web structure or the graphic appearance
- Incidents which have been caused by users in the original programs
- Computer equipment user´s setting
- Changes in web content: texts, images, etc.
- Incident diagnosis
- Solving System incidents
- Solving incident related with the web code

## License & Terms and conditions 
Grafana is an external publicly distributed visualization engine for time series data which it has been adapted to Synchronicity framework. Although, Grafana is not a tool developed by Synchronicity community, there are a plugin developed by HOP Ubiquitous, integrated and ready to be used into the Synchronicity framework. For that reason, license & terms and conditions should be those offered by Grafana for those development which has been done by Grafana community. In this link is explained all the details [enter here](https://github.com/grafana/grafana/blob/master/LICENSE.md.)

For the specific plugin (parking) which have been developed by HOP Ubiquitous to this Atomic service, it will be free for Partner and reference zones involved in synchronicity project through the main consortium or Open call, but their access will be limited to third parties external to synchronicity project as well as to their use in third projects. 

## Mantainers
- Rubén Molina, ruben@hopu.eu

<p align="center">
<img src="https://synchronicity-iot.eu/wp-content/uploads/2018/05/synchronicity_logo.svg" width=300/>
<img src="http://www.hopu.eu/wp-content/uploads/2016/05/HOP_LOGO_NEW_LINE.png" width=300/>
<p>
