# Grafana Atomic Service

# Introduction
This repository contains a **quick** way to deploy the Grafana Atomic Service
through docker compose and configuration of how to show devices on Grafana. To
full documentation, go to [complete guide](README.md)

# Architecture
TODO add image
The arrchitecture of this service, is simple. There are three components which
are responsible for transforming the data received from Orion to a graphic
format.
## High-level architecture
<br>

<br>

# Dependencies
  * docker
  * docker-compose
# Components
* QuantumLeap
* CrateDB
* Grafana

# Utils
* [Postman Collection](postman-collections)
* [docker-compose file](docker-compose/docker-compose.yml)
* [What is My IP?](https://www.whatismyip.com/)
* [Port
Forwarding](http://www.uttglobal.com/default.asp?id=3346#sthash.vwOCuYoE.dpbs)

# Setps for install

## 1. Docker Compose Deploy
Launch
```
$ docker-compose up
```
Stop
```
$ docker-compose down
```
Check alive
-   QuantumLeap:  [http://0.0.0.0:8668/v2/version](http://0.0.0.0:8668/v2/version)
-   Crate  [http://0.0.0.0:4200](http://0.0.0.0:4200)
-   Grafana:  [http://0.0.0.0:3000](http://0.0.0.0:3000) (Auth: Admin, Admin)
## 2. Port Forwarding
You need to open the port 8668 of main router to redirect incoming traffic over public IP to your private IP and port 8668 (QuantumLeap) of your host, know as port forwarding. It's necessary to permit receive updates from Orion to QuantumLeap deployed.
## 3. Create Subscription
Fields to change:
* Url variables:
	- {{url-orion}} IP or Url of Orion Context Broker, for this example, **fiware.hopu.eu**
	- {{port-orion}} Port where is Orion Context Broker, for this example, **1026**

* Headers variables:
	- {{fiware-service}} Name of fiware service, for this example, **SmartSpot**
	- {{fiware-servicepath}} Path of fiware service, for this example, **/smartspot**


* Body variables (not work with environment variables):
	- {{url-quantumleap}} IP or Url of QuantumLeap (Your Public IP)
	- {{port-quantumleap}} Port where is QuantumLeap: **8668**
  
<pre><code>curl -X POST \
  'http://<font color="red"><b>{{url-orion}}</b></font>:<font color="red"><b>{{port-orion}}</b></font>/v2/subscriptions' \
  -H 'Content-Type: application/json' \
  -H 'fiware-service: <font color="red"><b>{{fiware-service}}</b></font>' \
  -H 'fiware-servicepath: <font color="red"><b>{{fiware-servicepath}}</b></font>' \
  -d '{
   "description": "Create Orion to QuantumLeap subscription",
   "subject": {
       "entities": [
       {
           "idPattern": ".*",
           "type": "SmartSpot"
       }
       ],
       "condition": {
       "attrs": [
           "<font color="green">temperature</font>"
       ]
     }
   },
   "notification": {
       "http": {
           "url": "http://<font color="red"><b>{{url-public-quantumleap}}</b></font>:<font color="red"><b>{{port-public-quantumleap}}</b></font>/v2/notify"
       },
       "attrs": [
       "CO",
       "H2S",
       "NO2",
       "O3",
       "SO2",
       "humidity",
       "nearDevicesHour",
       "nearDevicesMinute",
       "nearDevicesTenMinutes",
       "temperature",
       "batteryStatus",
       "latitude",
       "longitude",
       "altitude",
       "speed",
       "batteryLevel",
       "noise"
       ],
       "metadata": ["dateCreated", "dateModified"]
   }
}'
</code></pre>

In example the condition of notify is when <font color="green">temperature</font> change.
> "condition": {"attrs": ["temperature"] }

You can add  more attributes here to change condition of notification, for example:

> "condition": {"attrs": ["temperature", "humidity"] }

### 4. Get and Delete Subscription
<pre><code>curl -X GET \
  'http://<font color="red"><b>{{url-orion}}</b></font>:<font color="red"><b>{{port-orion}}</b></font>/v2/subscriptions' \
  -H 'fiware-service: <font color="red"><b>{{fiware-service}}</b></font>' \
  -H 'fiware-servicepath: <font color="red"><b>{{fiware-servicepath}}</b></font>'
</pre></code>
Once you have **subscription id**, can remove the old subscription:
<pre><code>curl -X DELETE \
  'http://<font color="red"><b>{{url-orion}}</b></font>:<font color="red"><b>{{port-orion}}</b></font>/v2/subscriptions/<font color="red"><b>{{id-subscription}}</b></font>' \
  -H 'fiware-service: <font color="red"><b>{{fiware-service}}</b></font>' \
  -H 'fiware-servicepath: <font color="red"><b>{{fiware-servicepath}}</b></font>'
</pre></code>
## 5. Configure Grafana
Access Grafana and configure new data source with next fields:
- Name: Crate
- Type: Crate
- URL: http://localhost:4200
- Access: Browser / Direct
- Schema: mtsmartspot
- Table: etsmartspot
- Check Query Srouce: enabled
- Time column: time_index

## 6. Create Dashboard on Grafana
Create a new dashboard and select new graph. On bottom menu, choose Crate datasource and put for example this query:
![_images/sample_query.png](images/sample_query.png)

You must see:
![_images/sample_graph.png](images/sample_graph.png)
# Mantainers
- TODO

***
<p align="center">
<img src="https://synchronicity-iot.eu/wp-content/uploads/2018/05/synchronicity_logo.svg" width=300/>
<img src="http://www.hopu.eu/wp-content/uploads/2016/05/HOP_LOGO_NEW_LINE.png" width=300/>
<p>
